package com.everis.prototype.adapter;

import com.everis.prototype.model.Person;
import com.everis.prototype.service.ZombifyRequest;
import com.everis.prototype.service.ZombifyResponse;

import java.util.function.Function;

/**
 * Class to represent an example of the GenericAdapter implementation
 * Created by evinasgu - Everis SKL-1 on 05/12/2017.
 */
public class ZombifyAdapter extends GenericAdapter<Person, ZombifyRequest, ZombifyResponse> {

    /**
     * Constructor for ZombieAdapter class
     * @param firstDegreeTransformation
     * @param secondDegreeTransformation
     */
    public ZombifyAdapter(Function<Person, ZombifyRequest> firstDegreeTransformation,
                          Function<ZombifyRequest, ZombifyResponse> secondDegreeTransformation) {
        super(firstDegreeTransformation, secondDegreeTransformation);
    }
}
