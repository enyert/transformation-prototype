package com.everis.prototype.register;

import com.everis.prototype.model.Person;
import com.everis.prototype.service.ZombifyRequest;
import com.everis.prototype.service.ZombifyResponse;

import java.util.function.Function;

/**
 * This class contains the definition of all our transformations. Remember that one of the main features  of our transformations
 * is the capability to be reused in multiple flows
 * Created by evinasgu - Everis SKL-1 on 04/12/2017.
 */
public class TransformationRegister {

    public static final  Function<Person, ZombifyRequest> personToZombifyRequestTransformation = p -> new ZombifyRequest(p.toString());
    public static final Function<ZombifyRequest, ZombifyResponse> zombifyResponseTransformation = p -> {
      String encod = p.getExpression();
      return new ZombifyResponse(encod.split("-")[3], encod.split("-")[0]);
    };
}
