package com.everis.prototype.model;

/**
 * Created by evinasgu - Everis SKL-1 on 01/12/2017.
 */
public class Zombie {
    String id;
    String DNI;

    public Zombie(String id, String DNI) {
        this.id = id;
        this.DNI = DNI;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }
}
