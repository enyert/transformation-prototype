package com.everis.prototype.model;

/**
 * Created by evinasgu - Everis SKL-1 on 01/12/2017.
 */
public class Person {

    private String firstName;
    private String lastName;
    private String DNI;
    private Integer age;

    public Person(String firstName, String lastName, String DNI, Integer age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.DNI = DNI;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
    
    @Override
    public String toString() {
        return this.DNI + "-" + this.lastName + "-" + this.firstName + "-" + this.age;
    }
}
