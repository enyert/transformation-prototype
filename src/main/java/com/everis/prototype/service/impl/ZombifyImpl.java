package com.everis.prototype.service.impl;

import com.everis.prototype.adapter.ZombifyAdapter;
import com.everis.prototype.model.Person;
import com.everis.prototype.register.TransformationRegister;
import com.everis.prototype.service.Zombify;
import com.everis.prototype.service.ZombifyResponse;

/**
 * Created by evinasgu - Everis SKL-1 on 01/12/2017.
 */
public class ZombifyImpl implements Zombify {

    @Override
    public ZombifyResponse bite(Person input) {
        ZombifyAdapter zombifyAdapter = new ZombifyAdapter(
                TransformationRegister.personToZombifyRequestTransformation,
                TransformationRegister.zombifyResponseTransformation
        );
        return zombifyAdapter.apply(input);
    }
}
