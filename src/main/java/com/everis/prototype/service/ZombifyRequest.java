package com.everis.prototype.service;

/**
 * Created by evinasgu - Everis SKL-1 on 01/12/2017.
 */
public class ZombifyRequest {
    String expression;

    public ZombifyRequest(String expression) {
        this.expression = expression;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }
}
