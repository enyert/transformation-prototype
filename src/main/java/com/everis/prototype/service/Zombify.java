package com.everis.prototype.service;

import com.everis.prototype.model.Person;

/**
 * Created by evinasgu - Everis SKL-1 on 01/12/2017.
 */
public interface Zombify {
    ZombifyResponse bite(Person input);
}
