package com.everis.prototype.service;

/**
 * Created by evinasgu - Everis SKL-1 on 01/12/2017.
 */
public class ZombifyResponse {
    String age;
    String id;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ZombifyResponse(String age, String id) {

        this.age = age;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ZombifyResponse that = (ZombifyResponse) o;

        if (!getAge().equals(that.getAge())) return false;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        int result = getAge().hashCode();
        result = 31 * result + getId().hashCode();
        return result;
    }
}
