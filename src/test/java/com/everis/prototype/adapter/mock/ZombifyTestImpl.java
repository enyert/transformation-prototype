package com.everis.prototype.adapter.mock;

import com.everis.prototype.model.Person;
import com.everis.prototype.service.Zombify;
import com.everis.prototype.service.ZombifyResponse;

/**
 * Created by evinasgu - Everis SKL-1 on 06/12/2017.
 */
public class ZombifyTestImpl implements Zombify{
    @Override
    public ZombifyResponse bite(Person input) {
        return new ZombifyResponse("333", "11111111");
    }
}
