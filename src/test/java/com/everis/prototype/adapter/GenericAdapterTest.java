package com.everis.prototype.adapter;

import com.everis.prototype.model.Person;
import com.everis.prototype.register.TransformationRegister;
import com.everis.prototype.service.ZombifyRequest;
import com.everis.prototype.service.ZombifyResponse;
import com.everis.prototype.service.impl.ZombifyImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.function.Function;

/**
 * Created by evinasgu - Everis SKL-1 on 04/12/2017.
 */
public class GenericAdapterTest {

    ZombifyImpl zombify = new ZombifyImpl();

    Person testPerson;


    @Before
    public void setUp() {
        testPerson = new Person("Enyert", "Vinas", "11111111", 333);
    }

    @Test
    public void regularAdapterTest() {
        ZombifyResponse expectedResponse = new ZombifyResponse("333", "11111111");
        GenericAdapter<Person, ZombifyRequest, ZombifyResponse> testAdapter = new GenericAdapter<>(
                TransformationRegister.personToZombifyRequestTransformation,
                TransformationRegister.zombifyResponseTransformation);
        ZombifyResponse result = testAdapter.apply(testPerson);
        Assert.assertTrue(result.getId().equals(expectedResponse.getId()));
    }
}
