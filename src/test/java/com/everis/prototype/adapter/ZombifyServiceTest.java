package com.everis.prototype.adapter;

import com.everis.prototype.adapter.mock.ZombifyTestImpl;
import com.everis.prototype.model.Person;
import com.everis.prototype.service.ZombifyResponse;
import com.everis.prototype.service.impl.ZombifyImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by evinasgu - Everis SKL-1 on 06/12/2017.
 */
public class ZombifyServiceTest {

    Person testPerson;

    @Before
    public void setUp() throws Exception {
        testPerson = new Person("Enyert", "Vinas", "11111111", 333);
    }

    @Test
    public void zombifyTest() throws Exception {
        ZombifyTestImpl mockZombify = new ZombifyTestImpl(); //This could be done with DI but I'm not using Spring in this project
        ZombifyImpl zombify = new ZombifyImpl();
        ZombifyResponse result = zombify.bite(testPerson);
        ZombifyResponse expectedResult = mockZombify.bite(testPerson);

        Assert.assertEquals(result, expectedResult);
    }
}
