### transformation prototype project

    This project is an example of the usage of our Adapter implementation. 

#### Usage

    - Register the transformations in the TransformationRegister class
    - Use these transformations to create an specific adapter with the extension of the class
    GenericAdapter
    - Use the specific adapter created previously in our service implementation
    
Remember to make any idea to improve the project.

Long live for coders! 